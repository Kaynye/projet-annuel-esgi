build:
	docker-compose up -d --build
launch:
	docker-compose up -d
install:
	docker-compose run php composer install
migrate:
	docker-compose run php bin/console doctrine:migrations:migrate
lint:
	docker-compose run php bin/php-cs-fixer fix ./src
