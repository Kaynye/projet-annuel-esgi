<?php

namespace App\Controller\BackUser;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderController.
 *
 * @Route("/orders")
 */
class OrderController extends BaseController
{
    /**
     * @Route("/", name="orders_index", methods={"GET","POST"})
     */
    public function index(Request $request)
    {
        return $this->render('userAccount/orders.html.twig', [
            "user" => $this->getUser()
        ]);
    }
}
