<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 26/01/2020
 * Time: 22:30
 */

namespace App\Controller\Front;

use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Service\Cart\CartHelper;
use App\Service\OrderNumber;
use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/panier", name="cart_")
 */
class CartController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(SessionInterface $session, ProductRepository $productRepository)
    {
        if (!$session->has('panier')) {
            $session->set('panier', []);
        }


        $produits = $productRepository->findArray(array_keys($session->get('panier')));

        return $this->render('cart/panier.html.twig', [
            'produits' => $produits,
            'panier' => $session->get('panier'),
        ]);
    }

    /**
     * @Route("/validation", name="validation")
     */
    public function validate(CartHelper $cartHelper, OrderRepository $orderRepository, SessionInterface $session)
    {
        $prepareOrder = $cartHelper->prepareOrder();
        $order = $orderRepository->find($prepareOrder->getContent());


        return $this->render('Cart/validation.html.twig', [
            'order' => $order,
//            'panier' => $session->get('panier')
        ]);
    }

    /**
     * @Route("/validate", name="validate")
     */
    public function validation(OrderRepository $orderRepository, SessionInterface $session, Request $request, Mailer $mailer, OrderNumber $number)
    {
        $shopId = $request->query->get('id');
        $em = $this->getDoctrine()->getManager();
        $order = $orderRepository->find($shopId);
        if (!$order || $order->getValidate() == true) {
            throw $this->createNotFoundException();
        } else {
            $order->setValidate(true);
            $order->setOrderNumber($number->orderNumber());
            $em->flush();

            $session->remove('panier');
            $session->remove('order');

            $mailer->sendOrderMessage($order);

            $this->addFlash('success', 'Votre Commande a été validé avec succès');
            return $this->redirectToRoute('user_orders_index');
        }
    }

    /**
     * @Route("/show/{id}", name="show")
     */
    public function show(Product $product, SessionInterface $session)
    {
        if (!$product) {
            throw $this->createNotFoundException();
        }

        if ($session->has('panier')) {
            $panier = $session->get('panier');
        } else {
            $panier = false;
        }

        return $this->render('Front/presentation.html.twig', [
            'product' => $product,
            'panier' => $panier
        ]);
    }

    /**
     * @Route("/add/{id}", name="add")
     */
    public function add(CartHelper $cartHelper, $id)
    {
        $cartHelper->add($id);

        return $this->redirectToRoute('cart_index', [
            'id' => $id
        ]);
    }


    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(CartHelper $cartHelper, $id, SessionInterface $session)
    {
        $cartHelper->remove($id);
        return $this->redirectToRoute('cart_index', [
            'id' => $id
        ]);
    }
}
