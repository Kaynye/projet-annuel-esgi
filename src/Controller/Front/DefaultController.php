<?php

namespace App\Controller\Front;

use App\Entity\Shop;
use App\Repository\ShopRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('Front/index.html.twig');
    }

    /**
     * @Route("/shops", name="default_show")
     */
    public function show(ShopRepository $shopRepository)
    {
        $shops = $shopRepository->findOpensShops();

        return $this->render('Front/magasins.html.twig', [
            'shops' => $shops
        ]);
    }

    /**
     * @Route("/shop/{id}/products", name="default_products")
     */
    public function product(Shop $shop, SessionInterface $session)
    {
        if (!($shop->getStatus() === true)) {
            throw $this->createNotFoundException();
        }
        $produits = $shop->getProducts();

        if ($session->has('panier')) {
            $panier = $session->get('panier');
        } else {
            $panier = false;
        }
        return $this->render('Front/product.html.twig', [
            'shop' => $shop,
            'products' => $produits,
            'shopId' => $shop->getId(),
            'panier' => $panier
        ]);
    }


//    public function menu(SessionInterface $session)
//    {
//        if ($session->has('panier')) {
//            $nbProducts = 0;
//        }else {
//            $nbProducts = count($session->get('panier'));
//        }
//
//        return $this->render('Front/menu.html.twig', [
//            'nbProducts' => $nbProducts
//        ]);
//    }
}
