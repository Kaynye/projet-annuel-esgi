<?php

namespace App\Controller\Front;

use App\Entity\User;
use App\Form\PasswordForgotten;
use App\Form\PasswordReset;
use App\Form\UserRegistrationFormType;
use App\Security\LoginFormAuthenticator;
use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/register", name="app_register")
     */

    public function register(Request $request, Mailer $mailer, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $formAuthenticator)
    {
        $form = $this->createForm(UserRegistrationFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();

            $user->agreeTerms();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $mailer->sendWelcomeMessage($user);

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $formAuthenticator,
                'main'
            );
        }

        return $this->render('security/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/password-forgotten", name="app_password_forgotten", methods={"GET","POST"})
     */
    public function passwordForgotten(Request $request, Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {
        $form = $this->createForm(PasswordForgotten::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository('App\Entity\User')->findOneBy(['email' => $data['email']]);

            if ($user != null) {
                $token = $tokenGenerator->generateToken();

                $user->setResetToken($token);

                $em->flush();

                $url = $this->generateUrl('app_password_reset', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);

                $mailer->sendPasswordResetMail($user, $url);
            }
            return $this->render('security/ValidationMailSent.html.twig');
        }
        return $this->render('security/passwordForgotten.html.twig', [
            'passwordForgotten' => $form->createView(),
        ]);
    }

    /**
     * @Route("/password-reset", name="app_password_reset", methods={"GET","POST"})
     */
    public function passwordReset(Request $request)
    {
        $token = $request->query->get('token');
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('App\Entity\User')->findOneBy(['resetToken' => $token]);

        $form = $this->createForm(PasswordReset::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $user->setResetToken(null);

            $em->flush();
            $this->addFlash('success', 'Produit édité avec Succès');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/passwordReset.html.twig', [
            'passwordReset' => $form->createView(),
        ]);
    }
}
