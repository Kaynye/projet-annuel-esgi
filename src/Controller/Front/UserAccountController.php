<?php

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Form\UserAccountFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account")
 */
class UserAccountController extends BaseController
{
    /**
     * @Route("/", name="app_account", methods={"GET", "POST"})
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserAccountFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            if (true === $form['agreeTerms']->getData()) {
                $user->agreeTerms();
            }

            $user->setSubscribeToNewsletter($user->subscribeToNewsletter);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return $this->render('userAccount/index.html.twig', [
            'accountForm' => $form->createView(),
        ]);
    }
}
