<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 16/01/2020
 * Time: 22:04
 */

namespace App\Controller\BackOwner;

use App\Controller\BaseController;
use App\Entity\Product;
use App\Entity\ProductReference;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/product")
 */
class ProductReferenceAdminController extends BaseController
{
    /**
     * @Route("/{id}/references", name="admin_product_add_reference", methods={"POST"})
     */
    public function uploadProductArticleReference(Product $product, Request $request, UploaderHelper $uploaderHelper, EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('reference');

        $violations = $validator->validate(
            $uploadedFile,
            [
                new NotBlank([
                    'message' => 'Ajouter le fichier à télécharger'
                ]),
                new File([
                    'maxSize' => '5M',
                    'mimeTypes' => [
                         'image/*',
                         'application/pdf',
                        'application/msword',
                        'application/vnd.ms-excel',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                        'text/plain'
                    ]
                ]),
             ]
        );

        if ($violations->count() > 0) {
            return $this->json($violations, 400);
        }

        $filename = $uploaderHelper->uploadProductReference($uploadedFile);

        $productReference = new ProductReference($product);
        $productReference->setFilename($filename);
        $productReference->setOriginalFilename($uploadedFile->getClientOriginalName() ?? $filename);
        $productReference->setMimeType($uploadedFile->getMimeType() ?? 'application/octet-stream');

        $entityManager->persist($productReference);
        $entityManager->flush();

        return $this->json(
            $productReference,
            201,
            [],
            [
                    'groups' => ['main']
                ]
        );
    }

    /**
     * @Route("/{id}/references", name="admin_product_list_references", methods={"GET"})
     */
    public function getProductReferences(Product $product)
    {
        return $this->json(
            $product->getProductReferences(),
            200,
            [],
            [
                'groups' => ['main']
            ]
        );
    }

    /**
     * @Route("/references/{id}/download", name="admin_product_download_reference", methods={"GET"})
     */
    public function downloadProductReference(ProductReference $reference, UploaderHelper $uploaderHelper)
    {
        $response = new StreamedResponse(function () use ($reference, $uploaderHelper) {
            $outputStream = fopen('php://output', 'wb');
            $fileStram = $uploaderHelper->readStream($reference->getFilePath(), false);

            stream_copy_to_stream($fileStram, $outputStream);
        });

        $filenameFallBack = preg_replace('#^.*\.#', md5($reference->getOriginalFilename()). '.', $reference->getOriginalFilename());
        $response->headers->set('Content-Type', $reference->getMimeType());
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $reference->getOriginalFilename(),
            $filenameFallBack
        );

        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * @Route("/references/{id}", name="admin_product_delete_reference", methods={"DELETE"})
     */
    public function deleteProductReference(ProductReference $reference, UploaderHelper $uploaderHelper, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($reference);
        $entityManager->flush();

        $uploaderHelper->deleteFile($reference->getFilePath(), false);

        return new Response(null, 204);
    }

    /**
     * @Route("/references/{id}", name="admin_product_update_reference", methods={"PUT"})
     */
    public function updateProductReference(ProductReference $reference, UploaderHelper $uploaderHelper, EntityManagerInterface $entityManager, SerializerInterface $serializer, Request $request, ValidatorInterface $validator)
    {
        $serializer->deserialize(
            $request->getContent(),
            ProductReference::class,
            'json',
            [
                'object_to_populate' => $reference,
                'groups' => ['input']
            ]
        );

        $violations = $validator->validate($reference);
        if ($violations->count() > 0) {
            return $this->json($violations, 400);
        }

        $entityManager->persist($reference);
        $entityManager->flush();

        return $this->json(
            $reference,
            200,
            [],
            [
                'groups' => ['main']
            ]
        );
    }
}
