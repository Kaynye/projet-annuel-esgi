<?php

namespace App\Controller\BackOwner;

use App\Form\ShopStatus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $shop = $this->getUser()->getShops()->first();

        $form = $this->createForm(ShopStatus::class, $shop);

        $form->handleRequest($request);
        return $this->render('owner/index.html.twig', [
            'shopStatus' => $form
        ]);
    }
}
