<?php

namespace App\Controller\BackOwner;

use App\Entity\Shop;
use App\Entity\User;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ProductAdminController extends AbstractController
{
    /**
     * @Route("/admin/product", name="product_admin", methods={"POST", "GET"})
     */
    public function index(ProductRepository $repository, Request $request, PaginatorInterface $paginator, Security $security)
    {
        /** @var User $user */
        $user = $security->getUser();
//        $q = $request->query->get('q');
        $shopId = $request->query->get('id');
        $shop = $this->getDoctrine()->getRepository(Shop::class)->find($shopId);

        if ($shop->getUserAccount() != $user) {
            throw $this->createNotFoundException();
        }

//        $shops = $this->getUser()->getShops();

        $query = $repository->getProductsByShop($shop);
//        $queryBuilder = $repository->getWithSearchQueryBuilder($q);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('product_admin/index.html.twig', [
            'pagination' => $pagination,
            'shop' => $shop
        ]);
    }
}
