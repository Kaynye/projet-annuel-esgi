<?php

namespace App\Controller\BackOwner;

use App\Entity\Product;
use App\Entity\Shop;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Service\UploaderHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/product", name="product_")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(ProductRepository $productRepository, Security $security): Response
    {
        $user = $security->getUser();
        if (!$user) {
            throw $this->createNotFoundException();
        }

        return $this->render('product/index.html.twig', [
            'products' => $productRepository->getProductsByShop($user),
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request, UploaderHelper $uploaderHelper): Response
    {
        $shopId = $request->query->get('id');
        $shop = $this->getDoctrine()->getRepository(Shop::class)->find($shopId);

        $product = new Product();
        $product->setShop($shop);

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = ($form['imageFile']->getData());

            if ($uploadedFile) {
                $newFileName = $uploaderHelper->uploadProductImage($uploadedFile, $product->getPicture());

                $product->setPicture($newFileName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            $this->addFlash('success', 'Produit crée avec Succès');

            return $this->redirectToRoute('owner_product_admin', [
                'id' => $shop->getId()
            ]);
        }

        return $this->render('product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Product $product): Response
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     * 
     */
    // @IsGranted("MANAGE_PRODUCT", subject="product")
    public function edit(Request $request, Product $product, UploaderHelper $uploaderHelper): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = ($form['imageFile']->getData());

            if ($uploadedFile) {
                $newFileName = $uploaderHelper->uploadProductImage($uploadedFile, $product->getPicture());

                $product->setPicture($newFileName);
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Produit édité avec Succès');

            return $this->redirectToRoute('owner_product_admin', [
                'id' => $product->getShop()->getId()
            ]);
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Product $product): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_index');
    }
}
