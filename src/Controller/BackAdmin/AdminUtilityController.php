<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 14/01/2020
 * Time: 13:12
 */

namespace App\Controller\BackAdmin;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminUtilityController extends AbstractController
{
    /**
     * @Route("/utility/users", name="utility_users", methods={"GET"})
     */
    public function getUsersApi(UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findAllMatching($request->query->get('query'));

        return $this->json([
            'users' => $users
        ], 200, [], ['groups' => ['main']]);
    }
}
