<?php
namespace App\Controller\BackAdmin;

use App\Entity\Shop;
use App\Entity\User;
use App\Form\ShopType;
use App\Repository\ShopRepository;
use App\Service\UploaderHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/shop", name="shop_")
 */
class ShopController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(ShopRepository $shopRepository, Security $security): Response
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($security->isGranted('ROLE_SUPER_ADMIN')) {
            $shops = $shopRepository->findAll();
        } else {
            $shops = $shopRepository->findBy([
                'user_account' => $user
            ]);
        }

        return $this->render('shop/index.html.twig', [
            'shops' => $shops
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function new(Request $request, UploaderHelper $uploaderHelper): Response
    {
        $shop = new Shop();
        $form = $this->createForm(ShopType::class, $shop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = ($form['imageFile']->getData());

            if ($uploadedFile) {
                $newFileName = $uploaderHelper->uploadShopImage($uploadedFile, $shop->getPicture());
                $shop->getUserAccount()->setRoles(['ROLE_ADMIN']);
                $shop->setPicture($newFileName);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($shop);
            $entityManager->flush();

            return $this->redirectToRoute('shop_index');
        }

        return $this->render('shop/new.html.twig', [
            'shop' => $shop,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Shop $shop, Security $security): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user != $shop->getUserAccount() and !$security->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createNotFoundException();
        }
        return $this->render('shop/show.html.twig', [
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Shop $shop, UploaderHelper $uploaderHelper, Security $security): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user != $shop->getUserAccount() and !$security->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(ShopType::class, $shop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = ($form['imageFile']->getData());

            if ($uploadedFile) {
                $newFileName = $uploaderHelper->uploadShopImage($uploadedFile, $shop->getPicture());

                $shop->setPicture($newFileName);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('shop_index');
        }

        return $this->render('shop/edit.html.twig', [
            'shop' => $shop,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function delete(Request $request, Shop $shop): Response
    {
        if ($this->isCsrfTokenValid('delete'.$shop->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($shop);
            $entityManager->flush();
        }

        return $this->redirectToRoute('shop_index');
    }
}
