<?php


namespace App\Form\custom;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateTimePickerType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'input_options'  => [],
                'error_bubbling' => false,
        ]);
    }
    public function getBlockPrefix()
    {
        return 'app_datepicker_js';
    }
    public function getParent()
    {
        return DateType::class;
    }
}
