<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\custom\QuillJsType;
use App\Form\custom\DateTimePickerType;

use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $product = $options['data'] ?? null;
        $isEdit = $product && $product->getId();

        $builder
            ->add('name', TextType::class)
             ->add('description', TextareaType::class)
//            ->add('description', QuillJsType::class)
            ->add('price', MoneyType::class)
            ->add('isMenu', ChoiceType::class, [
                'placeholder' => 'Choices',
                'choices' => [
                    'Yes' => true,
                    'No' => false,
                ],
            ])
            ->add('quantity', IntegerType::class)
//            ->add('picture')
            ->add('publishedAt', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'placeholder' => 'choisir une catégorie'
            ]);

        $imageConstraints = [
                new Image([
                    'maxSize' => '2M'
                ]),
            ];

        if (!$isEdit || !$product->getPicture()) {
            $imageConstraints[] = new NotNull([
                    'message' => 'Ajouter une image s\'il vous plaît'
                ]);
        }
        $builder
                ->add('imageFile', FileType::class, [
                    'mapped' => false,
                    'required' => false,
                    'constraints' => $imageConstraints
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
