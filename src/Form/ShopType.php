<?php

namespace App\Form;

use App\Entity\Shop;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\custom\QuillJsType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

class ShopType extends AbstractType
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $shop = $options['data'] ?? null;
        $isEdit = $shop && $shop->getId();

        $builder
            ->add('name', TextType::class)
            ->add('phone', TelType::class)
            ->add('website', TelType::class)
            ->add('status', ChoiceType::class, [
                'placeholder' => 'Choices',
                'choices' => [
                    'Open' => true,
                    'Close' => false
                ]
            ])
//            ->add('description', QuillJsType::class)
            ->add('description', TextareaType::class)
            ->add('streetNumber', IntegerType::class)
            ->add('street', TextType::class)
            ->add('postalCode', TextType::class)
            ->add('city', TextType::class)
            ->add('userAccount', UserSelectTextType::class, [
                'disabled' => $isEdit
            ]);

        $imageConstraints = [
            new Image([
                'maxSize' => '2M'
            ]),
        ];

        if (!$isEdit || $shop->getPicture()) {
            $imageConstraints[] = new NotNull([
                'message' => 'Ajouter une image s\'il vous plaît'
            ]);
        }

        $builder
            ->add('imageFile', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => $imageConstraints
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shop::class,
        ]);
    }
}
