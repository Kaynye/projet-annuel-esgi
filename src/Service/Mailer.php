<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 10/02/2020
 * Time: 14:15
 */

namespace App\Service;

use App\Entity\Order;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class Mailer
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendWelcomeMessage(User $user): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->from(new Address('bricefouepe@hotmail.com', 'Restaurant'))
            ->to(new Address($user->getEmail(), $user->getFirstName()))
            ->subject('Bienvenue sur notre site')
            ->htmlTemplate('email/welcome.html.twig')
            ->context([
//                    'user' => $user
            ]);

        $this->mailer->send($email);

        return $email;
    }

    public function sendOrderMessage(Order $order)
    {
        $email = (new TemplatedEmail())
            ->from(new Address('bricefouepe@hotmail.com', 'Restaurant'))
            ->to(new Address($order->getUserAccount()->getEmail(), $order->getUserAccount()->getFirstName()))
//            ->to(new Address($user->getEmail(), $user->getFirstName()))
            ->subject('Commande')
            ->htmlTemplate('email/order.html.twig')
            ->context([
                'order' => $order
            ]);

        $this->mailer->send($email);

        return $email;
    }

    public function sendPasswordResetMail(User $user, String $url)
    {
        $email = (new TemplatedEmail())
            ->from(new Address('bricefouepe@hotmail.com', 'Restaurant'))
            ->to(new Address($user->getEmail(), $user->getFirstName()))
            ->subject('Reset password')
            ->htmlTemplate('email/resetPassword.html.twig')
            ->context([
               'user' => $user,
                'url' => $url
            ]);

        $this->mailer->send($email);

        return $email;
    }
}
