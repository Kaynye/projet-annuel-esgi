<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 15/01/2020
 * Time: 16:23
 */

namespace App\Service;

use Gedmo\Sluggable\Util\Urlizer;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper
{
    const PRODUCT_IMAGE = 'product_image';
    const MENU_IMAGE = 'menu_image';
    const SHOP_IMAGE = 'shop_image';

    const PRODUCT_REFERENCE = 'product_reference';

    private $filesystem;
    private $privateFilesystem;
    private $requestStackContext;
    private $logger;
    private $publicAssetBaseUrl;

//    RequestStackContext pour faire pointer l'image à la racine du projet et si jamais on deploie notre site dans un sous repertoire
    public function __construct(FilesystemInterface $publicUploadFilesystem, FilesystemInterface $privateUploadFilesystem, RequestStackContext $requestStackContext, LoggerInterface $logger, string $uploadedAssetsBaseUrl)
    {
        $this->filesystem = $publicUploadFilesystem;
        $this->requestStackContext = $requestStackContext;
        $this->logger = $logger;
        $this->publicAssetBaseUrl = $uploadedAssetsBaseUrl;
        $this->privateFilesystem = $privateUploadFilesystem;
    }

    public function uploadProductImage(File $file, ?string $existingFilename): string
    {
        $newFileName = $this->uploadFile($file, self::PRODUCT_IMAGE, true);

        if ($existingFilename) {
            try {
                $result = $this->filesystem->delete(self::PRODUCT_IMAGE.'/'.$existingFilename);

                if ($result === false) {
                    throw new \Exception(sprintf('vous ne pouvez pas ésupprimer l ancien fichier %s', $existingFilename));
                }
            } catch (FileNotFoundException $e) {
                $this->logger->alert(sprintf('Image téléchargée "%s" innexistante lorsque vous essayez de supprimer', $existingFilename));
            }
        }

        return $newFileName;
    }

    public function uploadMenuImage(File $file, ?string $existingFilename): string
    {
        $newFileName = $this->uploadFile($file, self::MENU_IMAGE, true);

        if ($existingFilename) {
            try {
                $result = $this->filesystem->delete(self::MENU_IMAGE.'/'.$existingFilename);

                if ($result === false) {
                    throw new \Exception(sprintf('vous ne pouvez pas ésupprimer l ancien fichier %s', $existingFilename));
                }
            } catch (FileNotFoundException $e) {
                $this->logger->alert(sprintf('Image téléchargée "%s" innexistante lorsque vous essayez de supprimer', $existingFilename));
            }
        }

        return $newFileName;
    }

    public function uploadShopImage(File $file, ?string $existingFilename): string
    {
        $newFileName = $this->uploadFile($file, self::SHOP_IMAGE, true);

        if ($existingFilename) {
            try {
                $result = $this->filesystem->delete(self::SHOP_IMAGE.'/'.$existingFilename);

                if ($result === false) {
                    throw new \Exception(sprintf('vous ne pouvez pas ésupprimer l ancien fichier %s', $existingFilename));
                }
            } catch (FileNotFoundException $e) {
                $this->logger->alert(sprintf('Image téléchargée "%s" innexistante lorsque vous essayez de supprimer', $existingFilename));
            }
        }

        return $newFileName;
    }


    public function uploadProductReference(File $file): string
    {
        return $this->uploadFile($file, self::PRODUCT_REFERENCE, false);
    }

    public function getPublicPath(string $path): string
    {
        return $this->requestStackContext->getBasePath().$this->publicAssetBaseUrl.'/'.$path;
    }

    /**
     * @return resource
     */
    public function readStream(string $path, bool $isPublic)
    {
        $filesystem = $isPublic ? $this->filesystem : $this->privateFilesystem;

        $resouce = $filesystem->readStream($path);

        if ($resouce === false) {
            throw new \Exception(sprintf('Error opening stream for "%s"', $path));
        }

        return $resouce;
    }

    public function deleteFile(string $path, bool $isPublic)
    {
        $filesystem = $isPublic ? $this->filesystem : $this->privateFilesystem;

        $result = $filesystem->delete($path);

        if ($result === false) {
            throw new \Exception(sprintf('Error deleting "%s"', $path));
        }
    }

    private function uploadFile(File $file, string $directory, bool $isPublic): string
    {
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename = $file->getFilename();
        }
        $newFilename = Urlizer::urlize(pathinfo($originalFilename, PATHINFO_FILENAME)).'-'.uniqid().'.'.$file->guessExtension();

        $filesystem = $isPublic ? $this->filesystem : $this->privateFilesystem;
        $stream = fopen($file->getPathname(), 'r');
        $result = $filesystem->writeStream(
            $directory.'/'.$newFilename,
            $stream
        );
        if ($result === false) {
            throw new \Exception(sprintf('Could not write uploaded file "%s"', $newFilename));
        }
        if (is_resource($stream)) {
            fclose($stream);
        }

        return $newFilename;
    }
}
