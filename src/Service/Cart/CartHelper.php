<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 27/01/2020
 * Time: 12:19
 */

namespace App\Service\Cart;

use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class CartHelper
{
    private $session;
    private $requestStack;
    private $entityManager;
    private $security;

    public function __construct(SessionInterface $session, RequestStack $requestStack, EntityManagerInterface $entityManager, Security $security)
    {
        $this->session = $session;
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function add($id)
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$this->session->has('panier')) {
            $this->session->set('panier', []);
        }

        $panier = $this->session->get('panier');


        if (isset($id, $panier)) {
            if ($request->query->get('qte') != null) {
                $panier[$id] = $request->query->get('qte');

                $this->session->getFlashBag()->add('success', 'Quantité modifiée avec succès');
            } else {
                if ($request->query->get('qte') != null) {
                    $panier[$id] = $request->query->get('qte');
                } else {
                    $panier[$id] = 1;
                }


                $this->session->getFlashBag()->add('success', 'Produit ajouté avec succès');
            }
        }

        $this->session->set('panier', $panier);
    }

    public function remove($id)
    {
        $panier = $this->session->get('panier', []);

        if (!empty($panier[$id])) {
            unset($panier[$id]);
        }

        $this->session->getFlashBag()->add('success', 'Produit supprimé avec succès');
        $this->session->set('panier', $panier);
    }


    public function item()
    {
        $productRepository = $this->entityManager->getRepository(Product::class);
        $panier = $this->session->get('panier');
        $item = [];
        $total = 0;
        $token = random_bytes(20);

        $produits = $productRepository->findArray(array_keys($this->session->get('panier')));

        foreach ($produits as $produit) {
            $price = $produit->getPrice() * $panier[$produit->getId()];
            $total += $price;

            $item['produit'][$produit->getId()] = [
                'reference' => $produit->getName(),
                'quantite' => $panier[$produit->getId()],
                'prix' => round($produit->getPrice(), 2),
            ];
        }

        $item['total'] = round($total, 2);
        $item['token'] =  bin2hex($token);
        return $item;
    }

    public function prepareOrder()
    {
        $repository = $this->entityManager->getRepository(Order::class);
        if (!$this->session->has('order')) {
            $order = new Order();
        } else {
            $order = $repository->find($this->session->get('order'));
        }

        $order->setDate(new \DateTime());
        $order->setUserAccount($this->security->getUser());
        $order->setValidate(0);
        $order->setOrderNumber(1);
        $order->setItem($this->item());

        if (!$this->session->has('order')) {
            $this->entityManager->persist($order);
            $this->session->set('order', $order);
        }

        $this->entityManager->flush();

        return new Response($order->getId());
    }
}
