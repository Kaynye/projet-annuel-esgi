<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 11/02/2020
 * Time: 12:57
 */

namespace App\Service;

use App\Repository\OrderRepository;

class OrderNumber
{
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function orderNumber()
    {
        $orderNumber = $this->repository->findByValidate();

        if (!$orderNumber) {
            return 1;
        } else {
            return $orderNumber[0]->getOrderNumber() + 1;
        }
    }
}
