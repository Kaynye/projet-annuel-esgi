<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Service\UploaderHelper;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class ProductFixtures extends BaseFixture implements DependentFixtureInterface
{
    private static $productName = [
        'Baguette',
        'croissant',
        'Pain au chocolat',
    ];

    private static $productPictures = [
        'test2.jpg',
        'test3.jpg',
        'test4.jpg',
    ];

    private $uploaderHelper;

    public function __construct(UploaderHelper $uploaderHelper)
    {
        $this->uploaderHelper = $uploaderHelper;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(20, 'main_products', function () {
            $product = new Product();
            $product->setName($this->faker->randomElement(self::$productName));
            $product->setDescription(
                $this->faker->boolean ? $this->faker->paragraph : $this->faker->sentences(2, true)
            );
            $product->setPrice($this->faker->numberBetween(10, 100));
            $product->setQuantity($this->faker->numberBetween(1, 10));
            $product->setIsMenu($this->faker->boolean(50));

            $picture = $this->fakeUploadImage();

            $product->setPicture($picture);
            if ($this->faker->boolean(70)) {
                $product->setPublishedAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            }

            $product->setIsDeleted($this->faker->boolean(20));

            $product->setShop($this->getRandomReference('main_shops'));
            $product->setCategory($this->getRandomReference('main_category'));


            return $product;
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ShopFixtures::class,
            CategoryFixtures::class
        ];
    }

    private function fakeUploadImage(): string
    {
        $randomImage = $this->faker->randomElement(self::$productPictures);
//            To copy images don't move
        $fs = new Filesystem();
        $targetPath = sys_get_temp_dir().'/'.$randomImage;
        $fs->copy(__DIR__.'/images/products/'.$randomImage, $targetPath, true);

        return $this->uploaderHelper->uploadProductImage(new File($targetPath), null);
    }
}
