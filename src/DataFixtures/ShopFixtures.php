<?php

namespace App\DataFixtures;

use App\Entity\Shop;
use App\Service\UploaderHelper;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class ShopFixtures extends BaseFixture implements DependentFixtureInterface
{
    private static $shopName = [
        'Boulangerie Brice',
        'Boulangerie Dui Dung',
        'Boulangerie Serigne',
        'Boulangerie Keany',
        'Boulangerie Martin',
    ];

    private static $shopPicture = [
        'boulangerie1.jpg',
        'boulangerie2.jpg',
        'boulangerie3.jpg',
    ];

    private $uploaderHelper;

    public function __construct(UploaderHelper  $uploaderHelper)
    {
        $this->uploaderHelper = $uploaderHelper;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(10, 'main_shops', function ($count) use ($manager) {
            $shop = new Shop();
            $shop->setName($this->faker->randomElement(self::$shopName));
            $shop->setDescription('Produits de qualité');
            $shop->setPhone($this->faker->phoneNumber);
            $shop->setStatus($this->faker->boolean(50));
            $shop->setWebsite(sprintf('www.monsite%d.com', $count));
            $shop->setStreetNumber($this->faker->randomNumber(3));
            $shop->setStreet($this->faker->streetName);
            $shop->setPostalCode($this->faker->postcode);
            $shop->setCity($this->faker->city);

            $picture = $this->fakeUploadImage();
            $shop->setPicture($picture);
            $shop->setUserAccount($this->getRandomReference('admin_users'));

            return $shop;
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }

    private function fakeUploadImage(): string
    {
        $randomImage = $this->faker->randomElement(self::$shopPicture);
        $fs = new Filesystem();
        $targetPath = sys_get_temp_dir().'/'.$randomImage;
        $fs->copy(__DIR__.'/images/shops/'.$randomImage, $targetPath, true);

        return $this->uploaderHelper->uploadShopImage(new File($targetPath), null);
    }
}
