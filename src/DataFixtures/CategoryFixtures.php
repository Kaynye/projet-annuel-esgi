<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 10/01/2020
 * Time: 12:52
 */

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends BaseFixture
{
    private static $categoryNames = [
        'Entrée',
        'Plat',
        'Dessert',
    ];

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(3, 'main_category', function () {
            $category = new Category();
            $category->setName($this->faker->unique()->randomElement(self::$categoryNames));

            return $category;
        });

        $manager->flush();
    }
}
