<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 25/12/2019
 * Time: 10:12
 */

namespace App\DataFixtures;

use App\Entity\Shop;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(10, 'main_users', function ($i) use ($manager) {
            $user = new User();
            $user->setFirstName($this->faker->firstName);
            $user->setLastName($this->faker->lastName);
            $user->setEmail(sprintf('test%d@test.com', $i));
            $user->setPlainPassword('test');
            $user->agreeTerms();
            if ($this->faker->boolean(75)) {
                $user->setSubscribeToNewsletter(true);
            }
            $user->setPhoneNumber($this->faker->phoneNumber);

//            /** @var Shop[] $shops */
//            $shops = $this->getRandomReferences('main_shops', $this->faker->numberBetween(0, 3));
//            foreach ($shops as $shop){
//                $user->addShop($shop);
//            }

            return $user;
        });

        $this->createMany(3, 'admin_users', function ($i) use ($manager) {
            $user = new User();
            $user->setFirstName($this->faker->firstName);
            $user->setLastName($this->faker->lastName);
            $user->setEmail(sprintf('admin%d@test.com', $i));
            $user->setPhoneNumber($this->faker->phoneNumber);
            $user->setRoles(['ROLE_ADMIN']);

            $user->setPlainPassword('test');

            $user->agreeTerms();

            return $user;
        });



        $this->createMany(2, 'super_admin_users', function ($i) use ($manager) {
            $user = new User();
            $user->setFirstName($this->faker->firstName);
            $user->setLastName($this->faker->lastName);
            $user->setEmail(sprintf('super_admin%d@test.com', $i));
            $user->setPhoneNumber($this->faker->phoneNumber);
            $user->setRoles(['ROLE_SUPER_ADMIN']);

            $user->setPlainPassword('test');

            $user->agreeTerms();

            return $user;
        });
        $manager->flush();
    }
}
