<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 10/01/2020
 * Time: 12:52
 */

namespace App\DataFixtures;

use App\Entity\Menu;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class OrderFixtures extends BaseFixture implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(3, 'main_orders', function () {
            $order = new Order();
            $order->setOrderNumber($this->faker->numberBetween(1000, 2000));
            $order->setValidate($this->faker->boolean(50));
            $order->setDate($this->faker->dateTimeBetween('-1 months', '-1 seconds'));
            $order->setUserAccount($this->getRandomReference('admin_users'));

            /** @var Product[] $products */
            $products = $this->getRandomReferences('main_products', $this->faker->numberBetween(1, 3));
            foreach ($products as $product) {
                $order->addProduct($product);
            }

            /** @var Menu[] $menus */
            $menus = $this->getRandomReferences('main_menus', $this->faker->numberBetween(1, 2));
            foreach ($menus as $menu) {
                $order->addMenu($menu);
            }

            return $order;
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ProductFixtures::class,
            MenuFixtures::class
        ];
    }
}
