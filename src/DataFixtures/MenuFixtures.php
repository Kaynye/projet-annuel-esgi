<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 10/01/2020
 * Time: 12:52
 */

namespace App\DataFixtures;

use App\Entity\Menu;
use App\Entity\Product;
use App\Service\UploaderHelper;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class MenuFixtures extends BaseFixture implements DependentFixtureInterface
{
    private static $menuNames = [
        'Salade, Poulet, Crudité',
        'Salade, Jambon cru, Chèvre',
        'Salade, Poulet',
    ];

    private static $menuPicture = [
        'menu1.jpg',
        'menu2.jpg',
        'menu3.jpg',
    ];

    private $uploaderHelper;

    public function __construct(UploaderHelper $uploaderHelper)
    {
        $this->uploaderHelper = $uploaderHelper;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(5, 'main_menus', function () {
            $menu = new Menu();
            $menu->setName($this->faker->randomElement(self::$menuNames));
            $menu->setPrice($this->faker->numberBetween(10, 100));
            $picture = $this->fakeUploadImage();
            $menu->setPicture($picture);
            $menu->setShop($this->getRandomReference('main_shops'));

            /** @var Product[] $products */
            $products = $this->getRandomReferences('main_products', $this->faker->numberBetween(1, 3));
            foreach ($products as $product) {
                $menu->addProduct($product);
            }

            return $menu;
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ShopFixtures::class,
            ProductFixtures::class
        ];
    }

    private function fakeUploadImage(): string
    {
        $randomImage = $this->faker->randomElement(self::$menuPicture);
        ////            To copy images don't move
        $fs = new Filesystem();
        $targetPath = sys_get_temp_dir().'/'.$randomImage;
        $fs->copy(__DIR__.'/images/menus/'.$randomImage, $targetPath, true);

        return $this->uploaderHelper->uploadMenuImage(new File($targetPath), null);
    }
}
