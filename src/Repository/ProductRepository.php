<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product[]
     */

    public function findAllPublishedOrderedByNewest()
    {
        return $this->addIsPublishedQueryBuilder()
            ->orderBy('p.publishedAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

//    /**
//     * @param null|string $term
//     */
//    public function getWithSearchQueryBuilder(?string $word): QueryBuilder
//    {
//        $qb = $this->createQueryBuilder('p')
//            ->innerJoin('p.shop', 's')
//            ->addSelect('s')
//        ;
//
//        if ($word) {
//            $qb->andWhere('LOWER(p.description) LIKE LOWER(:word) OR LOWER(p.name) LIKE LOWER(:word) OR LOWER(s.name) LIKE LOWER(:word)')
//                ->setParameter('word', '%'.$word.'%')
//            ;
//        }
//
//        return $qb
//            ->orderBy('p.createdAt', 'DESC');
//    }

//    public function getProductsByOwner($shop)
//    {
//        $qb = $this->createQueryBuilder('p');
//        if (count($shop) != 0) {
//            $qb->where('p.shop IN (:shops)')
//                ->setParameter('shops',$shop->toArray());
//        }
//
//        return $qb;
//    }

    public function getProductsByShop($shop)
    {
        $qb = $this->createQueryBuilder('p')
                    ->where('p.shop = :shop_id')
                    ->setParameter('shop_id', $shop)
                    ->getQuery();
        return $qb->getResult();
    }

    public function findByCategory($category)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.category = :category')
            ->orderBy('p.id')
            ->setParameter('category', $category)
            ->getQuery()
            ->getResult();
    }

    public function findArray($array)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.id IN (:array)')
            ->setParameter('array', $array)
            ->getQuery()
            ->getResult();
    }

//    public function findProductsByShopUser($user)
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.shop = :shop_id AND p.shop.user = : user_id')
//            ->setParameter('user', $user)
//            ->getQuery()
//            ->getResult();
//    }

//    public function searchProductsByCategory($category)
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.category = :category_id')
//            ->setParameter('category_id', $category)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    private function addIsPublishedQueryBuilder(QueryBuilder $qb = null)
    {
        return $this->getOrCreateQueryBuilder($qb)
            ->andWhere('p.publishedAt IS NOT NULL');
    }

    private function getOrCreateQueryBuilder(QueryBuilder $qb = null)
    {
        return $qb ?: $this->createQueryBuilder('p');
    }
}
