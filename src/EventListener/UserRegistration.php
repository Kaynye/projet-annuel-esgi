<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRegistration
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    public function prePersist(User $user, LifecycleEventArgs $args)
    {
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $user->getPlainPassword()
        ));
    }

    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    public function preUpdate(User $user, LifecycleEventArgs $args)
    {
        if ($user->getPlainPassword() != null) {
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $user->getPlainPassword()
            ));
        }
    }
}
