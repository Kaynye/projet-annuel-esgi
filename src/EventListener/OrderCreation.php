<?php


namespace App\EventListener;

use App\Entity\Order;
use App\Entity\User;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class OrderCreation
{
    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    public function prePersist(Order $order, LifecycleEventArgs $args)
    {
        $order->setDate(new \DateTime());
        $order->setValidate(false);

        $total = 0;

        $order->setPrice($total);
    }
}
