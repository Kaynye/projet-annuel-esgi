<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 31/01/2020
 * Time: 14:51
 */

namespace App\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class RedirectionListener
{
    private $router;
    private $session;
    private $security;

    public function __construct(RouterInterface $router, SessionInterface $session, Security $security)
    {
        $this->router = $router;
        $this->session = $session;
        $this->security = $security;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $route = $event->getRequest()->attributes->get('_route');

        if ($route == 'cart_validation') {
            if ($this->session->has('panier')) {
                if (count($this->session->get('panier')) == 0) {
                    $event->setResponse(new RedirectResponse(($this->router->generate('cart_validation'))));
                }
            }
            if (!$this->security->getUser()) {
                $this->session->getFlashBag()->add('notification', 'vous devez vous identifier');
                $event->setResponse(new RedirectResponse($this->router->generate('app_login')));
            }
        }
    }
}
